<?php

namespace Drupal\rollbar\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for rollbar settings.
 */
class RollbarSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'rollbar_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['rollbar.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('rollbar.settings');

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $config->get('enabled'),
    ];

    $form['access_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access Token (Server)'),
      '#default_value' => $config->get('access_token'),
      '#required' => TRUE,
    ];

    $form['access_token_frontend'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access Token (Client)'),
      '#default_value' => $config->get('access_token_frontend'),
      '#required' => TRUE,
    ];

    $form['capture_uncaught'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Capture Uncaught'),
      '#default_value' => $config->get('capture_uncaught'),
    ];

    $form['capture_unhandled_rejections'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Capture Uncaught Rejections'),
      '#default_value' => $config->get('capture_unhandled_rejections'),
    ];

    $form['environment'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Environment'),
      '#default_value' => $config->get('environment'),
      '#description' => $this->t('The environment string to use when reporting errors.'),
    ];

    $form['log_level'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Log Level'),
      '#default_value' => $config->get('log_level'),
      '#description' => $this->t('Selected log types will be send to Rollbar.'),
      '#options' => [
        'Emergency',
        'Alert',
        'Critical',
        'Error',
        'Warning',
        'Notice',
        'Info' ,
        'Debug',
      ]
    ];

    $form['channels'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Filter Channels'),
      '#default_value' => $config->get('channels'),
      '#description' => $this->t("Enter channels separated by ';' to prevent send them to Rollbar."),
    ];

    $form['rollbar_js_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rollbar JS URL'),
      '#default_value' => $config->get('rollbar_js_url'),
      '#description' => $this->t('The URL to the Rollbar js library.'),
    ];

    $form['host_white_list'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host White List'),
      '#default_value' => $config->get('host_white_list'),
      '#description' => $this->t('List of hosts for which Rollbar reports errors.'),
    ];

    $form['person_tracking'] = [
      '#type' => 'select',
      '#title' => $this->t('People Tracking'),
      '#default_value' => $config->get('person_tracking'),
      '#description' => $this->t('This associates errors with Drupal users. This may affect GDPR compliance.'),
      '#options' => [
        'off' => $this->t('None'),
        'id' => $this->t('User ID'),
        'on' => $this->t('Full'),
      ]
    ];

    $form['ignored_headers'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Ignored Headers'),
      '#default_value' => implode("\n", $config->get('ignored_headers') ?? []),
      '#description' => $this->t('Add custom HTTP headers here to be ignored., Enter one header per line, in the same style as in an HTTP request. For example %example', ['%example' => 'Custom-Header: custom-value']),
    ];

    $form['ignored_messages'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Ignored Messages'),
      '#default_value' => implode("\n", $config->get('ignored_messages') ?? []),
      '#description' => $this->t('Enter error messages separated by new lines to ignore them in Rollbar JS.'),
    ];

    $form['scrub_fields'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Scrub Fields'),
      '#default_value' => implode("\n", $config->get('scrub_fields') ?? [
        'passwd',
        'password',
        'secret',
        'confirm_password',
        'password_confirmation',
        'auth_token',
        'csrf_token',
      ]),
      '#description' => $this->t('Field names to scrub out of the entire payload. Enter one field name per line. Values will be replaced with asterisks.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('rollbar.settings')
      ->set('access_token', $form_state->getValue('access_token'))
      ->set('access_token_frontend', $form_state->getValue('access_token_frontend'))
      ->set('enabled', $form_state->getValue('enabled'))
      ->set('capture_uncaught', $form_state->getValue('capture_uncaught'))
      ->set('capture_unhandled_rejections', $form_state->getValue('capture_unhandled_rejections'))
      ->set('environment', $form_state->getValue('environment'))
      ->set('log_level', $form_state->getValue('log_level'))
      ->set('channels', $form_state->getValue('channels'))
      ->set('rollbar_js_url', $form_state->getValue('rollbar_js_url'))
      ->set('host_white_list', $form_state->getValue('host_white_list'))
      ->set('ignored_headers', !empty($form_state->getValue('ignored_headers')) ? preg_split("(\r\n?|\n)", $form_state->getValue('ignored_headers')) : [])
      ->set('ignored_messages', !empty($form_state->getValue('ignored_messages')) ? preg_split("(\r\n?|\n)", $form_state->getValue('ignored_messages')) : [])
      ->set('scrub_fields', !empty($form_state->getValue('scrub_fields')) ? preg_split("(\r\n?|\n)", $form_state->getValue('scrub_fields')) : [])
      ->set('person_tracking', $form_state->getValue('person_tracking'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
