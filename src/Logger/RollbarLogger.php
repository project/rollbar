<?php

namespace Drupal\rollbar\Logger;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LogMessageParserInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Session\AccountProxyInterface;
use Psr\Log\LoggerInterface;
use Drupal\Core\Logger\RfcLoggerTrait;
use Rollbar\Rollbar;
use Rollbar\Payload\Level as RollbarLogLevel;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Redirects logging messages to Rollbar.
 */
class RollbarLogger implements LoggerInterface {

  use RfcLoggerTrait;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * A configuration object containing rollbar settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The message's placeholders parser.
   *
   * @var \Drupal\Core\Logger\LogMessageParserInterface
   */
  protected $parser;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Checks if the Rollbar is initialized.
   *
   * @var bool
   */
  private $isInitialized = FALSE;

  /**
   * Constructs a Rollbar object.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *    The current user.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory object.
   * @param \Drupal\Core\Logger\LogMessageParserInterface $parser
   *   The parser to use when extracting message variables.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *    The request stack used to retrieve the current request.
   */
  public function __construct(AccountProxyInterface $account, ConfigFactoryInterface $config_factory, LogMessageParserInterface $parser, RequestStack $request_stack) {
    $this->account = $account;
    $this->config = $config_factory->get('rollbar.settings');
    $this->parser = $parser;
    $this->requestStack = $request_stack;
  }

  /**
   * Initialize rollbar object.
   */
  protected function init() {
    $token = $this->config->get('access_token');
    $environment = $this->config->get('environment');
    $enabled = $this->config->get('enabled');

    if (empty($token) || empty($environment) || !$enabled) {
      return FALSE;
    }

    if (!$this->isInitialized) {
      $config = [
        'access_token' => $token,
        'environment' => $environment,
        'scrub_fields' => $this->config->get('scrub_fields') ?: [],
      ];

      // Add Person Tracking if enabled
      if ($this->config->get('person_tracking') != 'off') {
        $config['person_fn']['id'] = $this->account->id();
        if ($this->config->get('person_tracking') == 'on'
          && $this->account->isAuthenticated()) {
          $config['capture_username'] = TRUE;
          $config['capture_email'] = TRUE;
          $config['person_fn']['username'] = $this->account->getAccountName();
          $config['person_fn']['email'] = $this->account->getEmail();
        }
      }

      // Check ignored headers and disable if necessary
      if ($headers = $this->config->get('ignored_headers')) {
        $request = $this->requestStack->getCurrentRequest();
        foreach ($headers as $header) {
          $split = explode(':', $header);
          if ($request->headers->get($split[0]) == trim($split[1])) {
            $config['enabled'] = FALSE;
          }
        }
      }

      Rollbar::init($config);
      $this->isInitialized = TRUE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, string|\Stringable $message, array $context = []): void {
    if (!$this->init()) {
      return;
    }
    $enabled_log_levels = $this->config->get('log_level');
    if (!is_array($enabled_log_levels)) {
        return;
    }
    $log_level_condition = !in_array($level, $enabled_log_levels);
    $omit_channel = array_map('trim', explode(";", $this->config->get('channels')));
    $omit_channel_condition = isset($context['channel']) && in_array($context['channel'], $omit_channel);
    if ($log_level_condition || $omit_channel_condition) {
        return;
    }
    $level_map = [
      RfcLogLevel::EMERGENCY => RollbarLogLevel::EMERGENCY,
      RfcLogLevel::ALERT =>  RollbarLogLevel::ALERT,
      RfcLogLevel::CRITICAL =>  RollbarLogLevel::CRITICAL,
      RfcLogLevel::ERROR =>  RollbarLogLevel::ERROR,
      RfcLogLevel::WARNING =>  RollbarLogLevel::WARNING,
      RfcLogLevel::NOTICE =>  RollbarLogLevel::NOTICE,
      RfcLogLevel::INFO =>  RollbarLogLevel::INFO,
      RfcLogLevel::DEBUG =>  RollbarLogLevel::DEBUG,
    ];

    // Populate the message placeholders and then replace them in the message.
    $message_placeholders = $this->parser->parseMessagePlaceholders($message, $context);
    $message = empty($message_placeholders) ? $message : strtr($message, $message_placeholders);

    // Make sure contents of the message variable are converted into string.
    Rollbar::log($level_map[$level], (string) $message, $context);
  }

}

